---
title: How to install PHP 7.3 on Ubuntu 18.04.
subtitle: Install php7.3.
date: 2019-08-02
tags: ["php"]
---

PHP 7.3 for Ubuntu and Debian is available from ondrej/php PPA repository. PHP 7.3 stable version has been released with many new features and bug fixes.

## Step 1: Add PHP 7.3 PPA

Add ondrej/php which has PHP 7.3 package and other required PHP extensions.

    sudo add-apt-repository ppa:ondrej/php
    sudo apt-get update

## Step 2: Install PHP 7.3

Once the PPA repository has been added, install php 7.3 on your Ubuntu 18.04 / Ubuntu 16.04 server.

    sudo apt-get install php7.3

## Step 3: Installing PHP 7.3 Extensions

Install PHP 7.3 extensions by using the syntax

    sudo apt-get install php7.3-<entension-name>

See example below

    sudo apt install php7.3-cli php7.3-fpm php7.3-json php7.3-pdo php7.3-mysql php7.3-zip php7.3-gd  php7.3-mbstring php7.3-curl php7.3-xml php7.3-bcmath php7.3-json
