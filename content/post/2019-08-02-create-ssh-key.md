---
title: How to create ssh key.
subtitle: Create ssh key.
date: 2019-08-02
tags: ["ssh"]
---

Run the command:

    ssh-keygen -t rsa -C "xxxxx@xxxxx.com"  
    # Generating public/private rsa key pair...

Press the enter key three times to generate ssh key.Get your public key by viewing the contents of the file `~/.ssh/id_rsa.pub`

    cat ~/.ssh/id_rsa.pub
    # ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6eNtGpNGwstc....